# intern-interview

Fake data and assignment for Project Evident education internship candidates over summer 2019.

## Assignment

The `data/` directory contains 2 (fake) data files, one of student demographic data, and one with pre- and post- test scores for those students, along with their assignment to a Treatment or Control group.

We would like an estimate of the impact of the treatment on post-test scores relative to the control group, controlling for covariates as appropriate. (There's no need to do anything too fancy, a simple model is all we're asking for.)

Please produce a *short* report in Word, preferably produced from RMarkdown, showing your code and addressing the following questions.

1. What is the average improvement from pre-test to post-test for the treatment group and the control group?
2. What is the estimated impact of the treatment on post-test scores, controlling for additional covariates in the student file?
3. What other covariates did you include in the model, and why? 
4. On this particular test, Hispanic students do better than Black students, and students who speak English at home do better than students who do not speak English at home. Expand on this in 1-3 sentences.

This is intended to be a *quick* way for you to demonstrate competency with R, statistics, and communication---please don't spend too much time on it, certainly no more than 3 hours, possibly less. The data is pretty clean (there are some repeated IDs and some missing values---don't worry much about those).